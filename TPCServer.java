package tcp;

import java.io.*;
import java.net.*;

public class TPCServer {

	public static void main(String argv []) throws Exception {
		String clientSentece;
		String capitalizedSetence;
		ServerSocket welcomeSocket = new ServerSocket(6789);
		Socket connectionSocket;
		BufferedReader inFromClient;
		while (true) {
			
			connectionSocket = welcomeSocket.accept();
			
			inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			
			clientSentece = inFromClient.readLine();
			capitalizedSetence = clientSentece.toUpperCase() + '\n';
			
			outToClient.writeBytes(capitalizedSetence);

		}
	}

}
