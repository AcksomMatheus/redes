package tcp;
import java.io.*;
import java.net.*;

public class TCPClient {

	public static void main(String[] args) throws Exception {
		String setence;
		String modifiedSentence;
		
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

		Socket clientSocket = new Socket("localhost", 6789);

		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		System.out.println("Escreva em minusculo");
		setence = inFromUser.readLine();

		outToServer.writeBytes(setence + '\n');

		modifiedSentence = inFromServer.readLine();

		System.out.println("FROM SERVER: " + modifiedSentence);

		clientSocket.close();
	}

}
